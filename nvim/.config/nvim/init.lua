vim.g.mapleader = " "

vim.o.relativenumber = true
vim.o.number = true
vim.o.hidden = true
vim.o.tabstop = 2
vim.o.shiftwidth = 2
vim.o.expandtab = true
vim.o.ignorecase = true
vim.o.smartcase = true
vim.o.mouse = "a"
vim.o.backspace = "indent,eol,start"
vim.o.completeopt = "menuone,noselect,noinsert"

vim.g.fileformats = "unix,dox"

-- Set powershell 7 as default windows terminal
if jit.os == 'Windows' then
  vim.o.shell = "pwsh"
end

if vim.fn.has("wsl") == 1 then
  vim.g.clipboard = {
    name = 'WslClipboard',
    copy = {
      ["+"] = "clip.exe",
      ["*"] = "clip.exe"
    },
    paste = {
      ["+"] = 'pwsh.exe -c [Console]::Out.Write($(Get-Clipboard -Raw).tostring().replace("`r", ""))',
      ["*"] = 'pwsh.exe -c [Console]::Out.Write($(Get-Clipboard -Raw).tostring().replace("`r", ""))'
    },
    cache_enabled = 0
  }
end

-- Goes to terminal mode immediately when entering terminal buffer
vim.api.nvim_create_autocmd('BufEnter', {
  pattern = 'term://*',
  callback = function()
    vim.cmd(":startinsert")
  end
})

local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

-- Plugins
require("lazy").setup({
  'kazhala/close-buffers.nvim',
  'ggandor/leap.nvim',
  'kyazdani42/nvim-web-devicons',
  'EdenEast/nightfox.nvim',
  'daschw/leaf.nvim',
  'kdheepak/lazygit.nvim',
  {
    'kyazdani42/nvim-tree.lua',
    dependencies = { 'kyazdani42/nvim-web-devicons' }
  },
  {
    "nvim-treesitter/nvim-treesitter",
    version = false, -- last release is way too old and doesn't work on Windows
    build = ":TSUpdate",
    event = { "BufReadPost", "BufWritePost", "BufNewFile", "VeryLazy" },
    init = function(plugin)
      -- PERF: add nvim-treesitter queries to the rtp and it's custom query predicates early
      -- This is needed because a bunch of plugins no longer `require("nvim-treesitter")`, which
      -- no longer trigger the **nvim-treeitter** module to be loaded in time.
      -- Luckily, the only thins that those plugins need are the custom queries, which we make available
      -- during startup.
      require("lazy.core.loader").add_to_rtp(plugin)
      require("nvim-treesitter.query_predicates")
    end,
    dependencies = {
      {
        "nvim-treesitter/nvim-treesitter-textobjects",
        config = function()
          -- When in diff mode, we want to use the default
          -- vim text objects c & C instead of the treesitter ones.
          local move = require("nvim-treesitter.textobjects.move") ---@type table<string,fun(...)>
          local configs = require("nvim-treesitter.configs")
          for name, fn in pairs(move) do
            if name:find("goto") == 1 then
              move[name] = function(q, ...)
                if vim.wo.diff then
                  local config = configs.get_module("textobjects.move")[name] ---@type table<string,string>
                  for key, query in pairs(config or {}) do
                    if q == query and key:find("[%]%[][cC]") then
                      vim.cmd("normal! " .. key)
                      return
                    end
                  end
                end
                return fn(q, ...)
              end
            end
          end
        end,
      },
    },
    cmd = { "TSUpdateSync", "TSUpdate", "TSInstall" },
    keys = {
      { "<c-space>", desc = "Increment selection" },
      { "<bs>",      desc = "Decrement selection", mode = "x" },
    },
    ---@type TSConfig
    ---@diagnostic disable-next-line: missing-fields
    opts = {
      highlight = { enable = true },
      -- indent = { enable = true },
      ensure_installed = {
        "bash",
        "c",
        "diff",
        "html",
        "javascript",
        "jsdoc",
        "json",
        "jsonc",
        "lua",
        "luadoc",
        "luap",
        "markdown",
        "markdown_inline",
        "python",
        "query",
        "regex",
        "toml",
        "tsx",
        "typescript",
        "vim",
        "vimdoc",
        "yaml",
      },
      incremental_selection = {
        enable = true,
        keymaps = {
          init_selection = "<C-space>",
          node_incremental = "<C-space>",
          scope_incremental = false,
          node_decremental = "<bs>",
        },
      },
      textobjects = {
        select = {
          enabled = true,
          keymaps = {
            ["af"] = { query = "@block.outer", desc = "Outer code block" },
            ["if"] = { query = "@block.inner", desc = "Inner code block" },
            ["ac"] = { query = "@class.outer", desc = "Select outer class" },
            ["ic"] = { query = "@class.inner", desc = "Select inner class" },
          }
        },
        swap = {
          enabled = true,
          swap_next = {
            ["<leader>a"] = { query = "@parameter.inner", desc = "Swap with next parameter" },
          },
          swap_previuos = {
            ["<leader>A"] = { query = "@parameter.inner", desc = "Swap with previous parameter" },
          },
        },
        move = {
          enable = true,
          goto_next_start = { ["]f"] = "@function.outer", ["]c"] = "@class.outer" },
          goto_next_end = { ["]F"] = "@function.outer", ["]C"] = "@class.outer" },
          goto_previous_start = { ["[f"] = "@function.outer", ["[c"] = "@class.outer" },
          goto_previous_end = { ["[F"] = "@function.outer", ["[C"] = "@class.outer" },
        },
      },
    },
    ---@param opts TSConfig
    config = function(_, opts)
      if type(opts.ensure_installed) == "table" then
        ---@type table<string, boolean>
        local added = {}
        opts.ensure_installed = vim.tbl_filter(function(lang)
          if added[lang] then
            return false
          end
          added[lang] = true
          return true
        end, opts.ensure_installed)
      end
      require("nvim-treesitter.configs").setup(opts)
    end,
  },
  {
    "kylechui/nvim-surround",
    version = "*", -- Use for stability; omit to use `main` branch for the latest features
    event = "VeryLazy",
    config = function()
      require("nvim-surround").setup()
    end
  },
  {
    'numToStr/Comment.nvim',
    lazy = false,
    config = function()
      require("Comment").setup()
    end
  },
  {
    'windwp/nvim-autopairs',
    event = "InsertEnter",
    opts = {} -- this is equalent to setup({}) function
  },
  { 'nvim-telescope/telescope.nvim', branch = '0.1.x' },
  'natecraddock/sessions.nvim',
  'natecraddock/workspaces.nvim',

  'neovim/nvim-lspconfig',

  'hrsh7th/cmp-nvim-lsp',
  'hrsh7th/cmp-buffer',
  'hrsh7th/cmp-path',
  'hrsh7th/cmp-cmdline',
  'hrsh7th/nvim-cmp',

  'L3MON4D3/LuaSnip',
  'saadparwaiz1/cmp_luasnip',

  {
    'nvim-lualine/lualine.nvim',
    dependencies = { 'kyazdani42/nvim-web-devicons', lazy = true }
  },

  'mfussenegger/nvim-dap',
  'nvim-lua/plenary.nvim',
  'nvim-pack/nvim-spectre',
  {
    'nvim-orgmode/orgmode',
    dependencies = {
      { 'nvim-treesitter/nvim-treesitter', lazy = true },
    },
    event = 'VeryLazy',
    config = function()

      -- Setup treesitter
      require('nvim-treesitter.configs').setup({
        highlight = {
          enable = true,
          additional_vim_regex_highlighting = { 'org' },
        },
        ensure_installed = { 'org' },
      })

      -- Setup orgmode
      require('orgmode').setup_ts_grammar()
      require('orgmode').setup({
        org_agenda_files = '~/myorgs/**/*',
        org_default_notes_file = '~/myorgs/refile.org',
        org_todo_keywords = {'TODO(t)', 'STARTED(s)', 'WAITING(w)', '|', 'DONE(d)', '|', 'CANCELLED(c)'},
        org_capture_templates = {
          j = {
            description = 'Journal',
            template = '\n* %<%Y-%m-%d> %<%A>\n\n** %U\n%?',
            target = '~/Sync/myorgs/journals/%<%Y-%m>.org'
          }
        }
      })
    end,
  },
  {
    "ellisonleao/glow.nvim",
    config = true,
    cmd = "Glow"
  },
  {
    'xemptuous/sqlua.nvim',
    lazy = true,
    cmd = 'SQLua',
    config = function() require('sqlua').setup() end
  },
  {
    "folke/which-key.nvim",
    event = "VeryLazy",

    -- init = function()
    --   vim.o.timeout = true
    --   vim.o.timeoutlen = 500
    -- end,
    --
    opts = {
      plugins = {
        registers = false,
        spelling = true,
      },
      defaults = {
        mode = { "n", "v" },
        ["<leader>f"] = { name = "+Find Files" },
        ["<leader>d"] = { name = "+Debug Menu" },
        ["<leader>g"] = { name = "+LSP" },
        ["<leader>s"] = { name = "+Search/Replace" },
        ["<localleader>"] = { name = "+Neorg" },
      }
    },
    config = function(_, opts)
      local wk = require("which-key")
      wk.setup(opts)
      wk.register(opts.defaults)
    end
  }
})

local parser_config = require "nvim-treesitter.parsers".get_parser_configs()
parser_config.blade = {
  install_info = {
    url = "https://github.com/EmranMR/tree-sitter-blade",
    files = { "src/parser.c" },
    branch = "main",
  },
  filetype = "blade"
}

require('leap').set_default_keymaps()

require('nightfox').setup {
  options = {
    dim_inactive = true,
  },
  -- Make inactive bg different from status line bg
  specs = {
    all = {
      inactive = "bg0"
    },
    nordfox = {
      inactive = "#292E39"
    }
  },
  groups = {
    all = {
      NormalNC = { bg = "inactive" }
    }
  }
}
vim.cmd('colorscheme nordfox')

local session = require('sessions')
session.setup {
  events = { "VimLeavePre" },
  session_filepath = '.nvim/session'
}
require('workspaces').setup {
  hooks = {
    add = function()
      session.save(nil, { silent = true })
    end,
    open = function()
      session.load(nil, { silent = true })
    end
  }
}


-- Custom keymappings
local map = vim.keymap.set
map('n', '<leader>/', '<cmd>nohlsearch<cr>', { desc = "Clear Search" })
for i = 1, 9, 1 do
  map('n', string.format('<A-%s>', i), string.format('<cmd>%stabnext<cr>', i))
  map('t', string.format('<A-%s>', i), string.format('<cmd>%stabnext<cr>', i))
end

for _, k in ipairs({ 'h', 'j', 'k', 'l' }) do
  local cmd = string.format('<A-%s>', k)
  local icmd = string.format('<C-\\><C-N><C-w>%s', k)

  map('t', cmd, icmd)
  map('i', cmd, icmd)
  map('n', cmd, string.format('<C-w>%s', k))
end

-- Easier navigation for wrapped text
map('n', 'j', 'gj', { noremap = true })
map('n', 'k', 'gk', { noremap = true })
map('n', 'gj', 'j', { noremap = true })
map('n', 'gk', 'k', { noremap = true })

-- LazyGit
map('n', '<leader>t', '<cmd>LazyGit<cr>', { noremap = true })

-- nvim-tree
local function nvim_tree_on_attach(bufnr)
  local api = require('nvim-tree.api')

  local function opts(desc)
    return { desc = 'nvim-tree: ' .. desc, buffer = bufnr, noremap = true, silent = true, nowait = true }
  end


  -- Default mappings. Feel free to modify or remove as you wish.
  --
  -- BEGIN_DEFAULT_ON_ATTACH
  vim.keymap.set('n', '<C-]>', api.tree.change_root_to_node, opts('CD'))
  vim.keymap.set('n', '<C-e>', api.node.open.replace_tree_buffer, opts('Open: In Place'))
  vim.keymap.set('n', '<C-k>', api.node.show_info_popup, opts('Info'))
  vim.keymap.set('n', '<C-r>', api.fs.rename_sub, opts('Rename: Omit Filename'))
  vim.keymap.set('n', '<C-t>', api.node.open.tab, opts('Open: New Tab'))
  vim.keymap.set('n', '<C-v>', api.node.open.vertical, opts('Open: Vertical Split'))
  vim.keymap.set('n', '<C-x>', api.node.open.horizontal, opts('Open: Horizontal Split'))
  vim.keymap.set('n', '<BS>', api.node.navigate.parent_close, opts('Close Directory'))
  vim.keymap.set('n', '<CR>', api.node.open.no_window_picker, opts('Open'))
  vim.keymap.set('n', '<Tab>', api.node.open.preview, opts('Open Preview'))
  vim.keymap.set('n', '>', api.node.navigate.sibling.next, opts('Next Sibling'))
  vim.keymap.set('n', '<', api.node.navigate.sibling.prev, opts('Previous Sibling'))
  vim.keymap.set('n', '.', api.node.run.cmd, opts('Run Command'))
  vim.keymap.set('n', '-', api.tree.change_root_to_parent, opts('Up'))
  vim.keymap.set('n', 'a', api.fs.create, opts('Create'))
  vim.keymap.set('n', 'bmv', api.marks.bulk.move, opts('Move Bookmarked'))
  vim.keymap.set('n', 'B', api.tree.toggle_no_buffer_filter, opts('Toggle No Buffer'))
  vim.keymap.set('n', 'c', api.fs.copy.node, opts('Copy'))
  vim.keymap.set('n', 'C', api.tree.toggle_git_clean_filter, opts('Toggle Git Clean'))
  vim.keymap.set('n', '[c', api.node.navigate.git.prev, opts('Prev Git'))
  vim.keymap.set('n', ']c', api.node.navigate.git.next, opts('Next Git'))
  vim.keymap.set('n', 'd', api.fs.remove, opts('Delete'))
  vim.keymap.set('n', 'D', api.fs.trash, opts('Trash'))
  vim.keymap.set('n', 'E', api.tree.expand_all, opts('Expand All'))
  vim.keymap.set('n', 'e', api.fs.rename_basename, opts('Rename: Basename'))
  vim.keymap.set('n', ']e', api.node.navigate.diagnostics.next, opts('Next Diagnostic'))
  vim.keymap.set('n', '[e', api.node.navigate.diagnostics.prev, opts('Prev Diagnostic'))
  vim.keymap.set('n', 'F', api.live_filter.clear, opts('Clean Filter'))
  vim.keymap.set('n', 'f', api.live_filter.start, opts('Filter'))
  vim.keymap.set('n', 'g?', api.tree.toggle_help, opts('Help'))
  vim.keymap.set('n', 'gy', api.fs.copy.absolute_path, opts('Copy Absolute Path'))
  vim.keymap.set('n', 'H', api.tree.toggle_hidden_filter, opts('Toggle Dotfiles'))
  vim.keymap.set('n', 'I', api.tree.toggle_gitignore_filter, opts('Toggle Git Ignore'))
  vim.keymap.set('n', 'J', api.node.navigate.sibling.last, opts('Last Sibling'))
  vim.keymap.set('n', 'K', api.node.navigate.sibling.first, opts('First Sibling'))
  vim.keymap.set('n', 'm', api.marks.toggle, opts('Toggle Bookmark'))
  vim.keymap.set('n', 'o', api.node.open.edit, opts('Open'))
  vim.keymap.set('n', 'O', api.node.open.no_window_picker, opts('Open: No Window Picker'))
  vim.keymap.set('n', 'p', api.fs.paste, opts('Paste'))
  vim.keymap.set('n', 'P', api.node.navigate.parent, opts('Parent Directory'))
  vim.keymap.set('n', 'q', api.tree.close, opts('Close'))
  vim.keymap.set('n', 'r', api.fs.rename, opts('Rename'))
  vim.keymap.set('n', 'R', api.tree.reload, opts('Refresh'))
  vim.keymap.set('n', 's', api.node.run.system, opts('Run System'))
  vim.keymap.set('n', 'S', api.tree.search_node, opts('Search'))
  vim.keymap.set('n', 'U', api.tree.toggle_custom_filter, opts('Toggle Hidden'))
  vim.keymap.set('n', 'W', api.tree.collapse_all, opts('Collapse'))
  vim.keymap.set('n', 'x', api.fs.cut, opts('Cut'))
  vim.keymap.set('n', 'y', api.fs.copy.filename, opts('Copy Name'))
  vim.keymap.set('n', 'Y', api.fs.copy.relative_path, opts('Copy Relative Path'))
  vim.keymap.set('n', '<2-LeftMouse>', api.node.open.edit, opts('Open'))
  vim.keymap.set('n', '<2-RightMouse>', api.tree.change_root_to_node, opts('CD'))
  -- END_DEFAULT_ON_ATTACH


  -- Mappings migrated from view.mappings.list
  --
  -- You will need to insert "your code goes here" for any mappings with a custom action_cb
end

require("nvim-tree").setup {
  actions = {
    open_file = {
      quit_on_open = true,
      window_picker = {
        enable = false
      }
    }
  },
  view = {
    width = '30%'
  },
  on_attach = nvim_tree_on_attach
}

map('n', '<leader>n', '<cmd>NvimTreeToggle<cr>', { desc = "Browse Files" })
map('n', '<leader>N', '<cmd>NvimTreeFindFile<cr>', { desc = "Browse Current File" })


-- Telescope related configs
local telescope = require('telescope')
local actions = require('telescope.actions')
telescope.load_extension('workspaces')
telescope.setup {
  defaults = {
    mappings = {
      i = {
        ["<C-h>"] = "which_key",
      }
    },
    layout_strategy = 'vertical',
    vimgrep_arguments = { 'rg', '--ignore-file=.gitignore',
      '--color=never', '--no-heading', '--with-filename',
      '--line-number', '--column', '--smart-case', '--trim' },
    preview = {
      filesize_limit = 0.5
    }
  },
  pickers = {
    find_files = {
      find_command = { 'fd', '--ignore-file', '.gitignore', '--type', 'f' }
    },
    buffers = {
      mappings = {
        n = {
          ["d"] = actions.delete_buffer
        }
      }
    }
  },
}

local ts_builtin = require('telescope.builtin')


map('n', '<leader>ff', function() ts_builtin.find_files({ shorten_path = true }) end, { desc = "Search Files" })
map('n', '<leader>fh', function() ts_builtin.find_files({ shorten_path = true, hidden = true }) end,
  { desc = "Search Files (Include Hidden)" })
map('n', '<leader>fg', ts_builtin.live_grep, { desc = "Search in files" })
map('n', '<leader>f*', ts_builtin.grep_string, { desc = "Search cursor keyword in files" })
map('n', ';', function() ts_builtin.buffers({ sort_mru = true }) end, { desc = "Search Buffers" })
map('n', '<leader>fs', telescope.extensions.workspaces.workspaces, { desc = "Search Sessions" })

--
-- Auto completion setup
local luasnip = require 'luasnip'
local cmp = require 'cmp'
cmp.setup {
  snippet = {
    expand = function(args)
      luasnip.lsp_expand(args.body)
    end,
  },
  mapping = cmp.mapping.preset.insert({
    ['<C-u>'] = cmp.mapping.scroll_docs(-4), -- Up
    ['<C-d>'] = cmp.mapping.scroll_docs(4),  -- Down
    -- C-b (back) C-f (forward) for snippet placeholder navigation.
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<C-e>'] = cmp.mapping.abort(),
    ['<CR>'] = cmp.mapping.confirm {
      behavior = cmp.ConfirmBehavior.Replace,
      select = true,
    },
    ['<Tab>'] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      elseif luasnip.expand_or_jumpable() then
        luasnip.expand_or_jump()
      else
        fallback()
      end
    end, { 'i', 's' }),
    ['<S-Tab>'] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      elseif luasnip.jumpable(-1) then
        luasnip.jump(-1)
      else
        fallback()
      end
    end, { 'i', 's' }),
  }),
  sources = cmp.config.sources({
    { name = 'nvim_lsp' },
    { name = 'luasnip' },
    { name = 'path' },
    { name = 'orgmode' },
  }, {
    { name = 'buffer' }
  }),
}

-- Autopair integration
local cmp_autopairs = require('nvim-autopairs.completion.cmp')
cmp.event:on(
  'confirm_done',
  cmp_autopairs.on_confirm_done()
)

-- LSP setup
local nvim_lsp = require('lspconfig')

local capabilities = require("cmp_nvim_lsp").default_capabilities()

vim.diagnostic.config({
  virtual_text = false
})

--[[ function list_ws_folders()
  print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
end ]]

vim.api.nvim_create_autocmd('LspAttach', {
  group = vim.api.nvim_create_augroup('UserLspConfig', {}),
  callback = function(ev)
    vim.bo[ev.buf].omnifunc = 'v:lua.vim.lsp.omnifunc'
    map('n', '<leader>gD', vim.lsp.buf.declaration, { buffer = ev.buf, desc = "Declaration" })
    map('n', '<leader>gd', vim.lsp.buf.definition, { buffer = ev.buf, desc = "Definition" })
    map('n', '<leader>gK', vim.lsp.buf.hover, { buffer = ev.buf, desc = "Hover" })
    map('n', '<leader>gk', vim.lsp.buf.signature_help, { buffer = ev.buf, desc = "Signature Help" })
    map('n', '<leader>gi', vim.lsp.buf.implementation, { buffer = ev.buf, desc = "Implementation" })
    --[[ map('n', '<leader>gw', vim.lsp.buf.add_workspace_folder, { buffer = ev.buf, desc="Add Workspace Folder" })
    map('n', '<leader>gW', vim.lsp.buf.remove_workspace_folder, { buffer = ev.buf, desc="Remove Workspace Folder" })
    map('n', '<leader>gl', list_ws_folders, { buffer = ev.buf, desc="List Workspace Folders" }) ]]
    map('n', '<leader>gt', vim.lsp.buf.type_definition, { buffer = ev.buf, desc = "Type Definition" })
    map('n', '<leader>gn', vim.lsp.buf.rename, { buffer = ev.buf, desc = "Rename" })
    map('n', '<leader>gc', vim.lsp.buf.code_action, { buffer = ev.buf, desc = "Code Actions" })
    map('n', '<leader>gr', ts_builtin.lsp_references, { buffer = ev.buf, desc = "References" })
    map('n', '<leader>gf', vim.lsp.buf.format, { buffer = ev.buf, desc = "Format" })
  end

})

map('n', '<leader>ge', vim.diagnostic.open_float, { desc = "Diagnostics in float window" })
map('n', '<leader>g[', vim.diagnostic.goto_prev, { desc = "Prev Diagnostic" })
map('n', '<leader>g]', vim.diagnostic.goto_next, { desc = "Next Diagnostic" })
map('n', '<leader>gq', vim.diagnostic.setloclist, { desc = "Diagnistics in location list" })

nvim_lsp.html.setup { capabilities = capabilities }
nvim_lsp.clangd.setup { capabilities = capabilities }
nvim_lsp.tsserver.setup { capabilities = capabilities }
nvim_lsp.ghcide.setup { capabilities = capabilities }
nvim_lsp.pyright.setup { capabilities = capabilities }
nvim_lsp.quick_lint_js.setup { capabilities = capabilities }

local elixirls_path = "/home/nayminlwin/elixir-ls/language_server.sh"
if jit.os == 'Windows' then
  elixirls_path = "C:/Users/Nay Min Lwin/elixir-ls/language_server.bat"
end
nvim_lsp.elixirls.setup {
  cmd = { elixirls_path },
  capabilities = capabilities
}

nvim_lsp.rust_analyzer.setup {
  settings = {
    ['rust-analyzer'] = {
      checkOnSave = {
        enable = false
      },
      cargo = {
        extraEnv = {
          CARGO_TARGET_DIR = "target/analyzer"
        },
        extraArgs = { "--profile", "rust-analyzer" }
      }
    }
  },
  capabilities = capabilities
}

nvim_lsp.dartls.setup { capabilities = capabilities }
nvim_lsp.svelte.setup { capabilities = capabilities }

nvim_lsp.lua_ls.setup {
  capabilities = capabilities,
  settings = {
    Lua = {
      runtime = {
        -- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
        version = 'LuaJIT',
      },
      diagnostics = {
        -- Get the language server to recognize the `vim` global
        globals = { 'vim' },
      },
      workspace = {
        -- Make the server aware of Neovim runtime files
        library = vim.api.nvim_get_runtime_file("", true),
      },
      -- Do not send telemetry data containing a randomized but unique identifier
      telemetry = {
        enable = false,
      },
    },
  },
}

nvim_lsp.phpactor.setup { capabilities = capabilities }
nvim_lsp.zls.setup { capabilities = capabilities }
nvim_lsp.vuels.setup { capabilities = capabilities }

nvim_lsp.vuels.setup { capabilities = capabilities }

-- DAP setup
local dap = require 'dap'

dap.set_log_level('TRACE')

map('n', '<F5>', dap.continue)
map('n', '<F10>', dap.step_over)
map('n', '<F11>', dap.step_into)
map('n', '<F12>', dap.step_out)
map('n', '<leader>db', dap.toggle_breakpoint, { desc = "Toggle Breakpoint" })
map('n', '<leader>dB', function() dap.set_breakpoint(vim.fn.input("Breakpoint condition: ")) end,
  { desc = "Breakpoint Condition" })
map('n', '<leader>dp', function() dap.set_breakpoint(nil, nil, vim.fn.input('Log point message: ')) end,
  { desc = "Log point" })
map('n', '<leader>dr', dap.repl.open, { desc = "REPL" })
map('n', '<leader>dl', dap.run_last, { desc = "Run Last Debug" })
map('n', '<leader>ds', dap.terminate, { desc = "Terminate Debug" })

dap.adapters.mix_task = {
  type = 'executable',
  command = "/home/nayminlwin/elixir-ls/debugger.sh",
  args = {}
}

dap.configurations.elixir = {
  {
    type = "mix_task",
    name = "phx.server",
    request = "launch",
    task = "phx.server",
    projectDir = "${workspaceFolder}"
  },
  {
    type = "mix_task",
    name = "mix test",
    request = "launch",
    task = "test",
    taskArgs = { "--trace" },
    projectDir = "${workspaceFolder}",
    requireFiles = {
      "test/**/test_helper.exs",
      "test/**/*_test.exs"
    }
  },
}

local spectre = require 'spectre'
spectre.setup {
  open_cmd = 'noswapfile tabnew'
}
map('n', '<leader>sr', spectre.open, { desc = "Search/Replace (Spectre)" })

require('lualine').setup {
  sections = {
    lualine_c = {
      {
        'filename', path = 1,
      }
    }
  }
}
